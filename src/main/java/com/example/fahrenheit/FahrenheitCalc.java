package com.example.fahrenheit;

import java.text.ParseException;

public class FahrenheitCalc {

    public int calculateTemperature(String temperature) {
        if (!isParsable(temperature)){
            return 0;
        }
        int temp = Integer.parseInt(temperature);
        //calculation
        double fahrenheit = (temp * 1.8) + 32;
        int finalFahrenheit = (int) Math.round(fahrenheit);
        return finalFahrenheit;
    }

    public static boolean isParsable(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

}
