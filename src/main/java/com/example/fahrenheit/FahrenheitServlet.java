package com.example.fahrenheit;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class FahrenheitServlet extends HttpServlet {
    private FahrenheitCalc calculator;

    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
        calculator = new FahrenheitCalc();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Fahrenheit calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("temp") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                Double.toString(calculator.calculateTemperature(request.getParameter("temp"))) +
                "</BODY></HTML>");
    }
}